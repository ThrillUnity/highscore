require 'test_helper'

class GameTest < ActiveSupport::TestCase
  test "should not save game without name" do
    game = Game.new

    assert_not game.save, "Saved the game without a name"
  end

  test "should not save game if name is already taken (case-sensitive)" do
    error_message = "Saved the game with name already taken"

    game      = Game.new
    game.name = "Pacman"
    game.save

    game      = Game.new
    game.name = "Pacman"
    assert_not game.save, error_message

    # case-sensitive
    game      = Game.new
    game.name = "pacman"
    assert_not game.save, error_message
  end

end
