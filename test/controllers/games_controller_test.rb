require 'test_helper'

class GamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @game = games(:pacman)
  end

  test "should get index" do
    get v1_games_url, as: :json
    assert_response :success
  end

  test "should create game" do
    assert_difference('Game.count') do
      post v1_games_url, params: { game: { name: "Test" } }, as: :json
    end

    assert_response 201
  end

  test "should show game" do
    get v1_game_url(@game), as: :json
    assert_response :success
  end

  test "should update game" do
    patch v1_game_url(@game), params: { game: { name: @game.name } }, as: :json
    assert_response 200
  end

  test "should destroy game" do
    assert_difference('Game.count', -1) do
      delete v1_game_url(@game), as: :json
    end

    assert_response 204
  end
end
