Rails.application.routes.draw do

  scope module: 'api' do
    namespace :v1 do
      resources :games do
        resources :scores
      end

      resources :players do
        resources :scores
      end

      resources :scores
    end
  end
end
