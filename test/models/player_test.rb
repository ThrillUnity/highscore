require 'test_helper'

class PlayerTest < ActiveSupport::TestCase
  player_name  = "Billy Mitchell"
  player_email = "billy.mitchell@pacman.com"

  test "should not save player without name and email" do
    player = Player.new

    assert_not player.save, "Saved the player without a name and a email"
    assert player.errors.size == 2
  end

  test "should not save player if name is already taken (case-sensitive)" do
    error_message = "Saved the player with name already taken"

    player      = Player.new
    player.name = player_name
    player.save

    player      = Player.new
    player.name = player_name
    assert_not player.save, error_message

    # case-sensitive
    player      = Player.new
    player.name = player_name.downcase
    assert_not player.save, error_message
  end

  test "should not save player if email is already taken (case-sensitive)" do
    error_message = "Saved the player with email already taken"

    player       = Player.new
    player.email = player_email
    player.save

    player       = Player.new
    player.email = player_email
    assert_not player.save, error_message

    # case-sensitive
    player       = Player.new
    player.email = player_email.upcase
    assert_not player.save, error_message
  end
end
