require 'test_helper'

class ScoreTest < ActiveSupport::TestCase
  test "should not save score without points, player and game" do
    score = Score.new

    assert_not score.save, "Saved the score without points, player and game"
    assert score.errors.messages.size == 5
  end
end
