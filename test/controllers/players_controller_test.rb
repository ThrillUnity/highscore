require 'test_helper'

class PlayersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @player = players(:billy)
  end

  test "should get index" do
    get v1_players_url, as: :json
    assert_response :success
  end

  test "should create player" do
    assert_difference('Player.count') do
      post v1_players_url, params: { player: { email: "player1@gmail.com", name: "Player1" } }, as: :json
    end

    assert_response 201
  end

  test "should show player" do
    get v1_player_url(@player), as: :json
    assert_response :success
  end

  test "should update player" do
    patch v1_player_url(@player), params: { player: { email: @player.email, name: @player.name } }, as: :json
    assert_response 200
  end

  test "should destroy player" do
    assert_difference('Player.count', -1) do
      delete v1_player_url(@player), as: :json
    end

    assert_response 204
  end
end
